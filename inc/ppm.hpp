#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

class PPM {
    private:
        unsigned char** r;
        unsigned char** g;
        unsigned char** b;
        string magicnumber;       
        int altura;
        int largura;        
        int maxcolor;
        int imagesize;

    public:
        PPM();
        ~PPM();
        char getImagem();
        unsigned char** getR();
        unsigned char** getG();
        unsigned char** getB();
        string getMagicNumber();
        int getAltura();
        int getLargura();
        int getImageSize();
        int getTotalSize();
        int getHeaderSize();
        int getMaxColor();
        void setImagem(char *imagem);
        void setR(unsigned char** r);
        void setG(unsigned char** g);
        void setB(unsigned char** b);
        void setMagicNumber(string magicnumber);
        void setAltura(int altura);
        void setLargura(int largura);
        void setImageSize(int imagesize);
        void setMaxColor(int maxcolor);
        void read();
        void filtro_negativo();
        void filtro_polarizado();
        void filtro_pretoebranco();
        //void filtro_blur();
};

#endif
