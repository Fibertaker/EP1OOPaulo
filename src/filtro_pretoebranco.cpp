#include "ppm.hpp"

using namespace std;

void PPM::filtro_pretoebranco(){

	int grayscale_value;
 	string foto,nome;
    char blank='\n';

	cout << "Digite o nome de saída da imagem: " << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f3(nome.c_str());

    f3 << getMagicNumber();
    f3.put(blank);
    f3 << getLargura();
 	f3.put(blank);
 	f3 << getAltura();
    f3.put(blank);
    f3 << getMaxColor();
    f3.put(blank);

	for(int i=0; i<altura; i++){
    	for(int j=0; j<largura; j++){
            grayscale_value = (0.299 * r[i][j]) + (0.587 * g[i][j]) + (0.144 * b[i][j]);

            r[i][j] = grayscale_value;
		    f3.write((char*)&r[i][j],1);

            g[i][j] = grayscale_value;
			f3.write((char*)&g[i][j],1);

            b[i][j] = grayscale_value;
			f3.write((char*)&b[i][j],1);
    	}
	}
	f3.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
