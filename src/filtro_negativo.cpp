#include "ppm.hpp"

using namespace std;

void PPM::filtro_negativo(){

 	string foto,nome;
    char blank='\n';

	cout << "Digite o nome de saída da imagem: " << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f1(nome.c_str());

    f1 << getMagicNumber();
    f1.put(blank);
    f1 << getLargura();
 	f1.put(blank);
 	f1 << getAltura();
    f1.put(blank);
    f1 << getMaxColor();
    f1.put(blank);

	for(int i=0;i<altura;i++){
		for(int j=0;j<largura;j++){

			r[i][j] = maxcolor - (int)r[i][j];
			f1.write((char*)&r[i][j],1);

			g[i][j] = maxcolor - (int)g[i][j];
			f1.write((char*)&g[i][j],1);

			b[i][j] = maxcolor - (int)b[i][j];
			f1.write((char*)&b[i][j],1);
		}
	}
	f1.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
