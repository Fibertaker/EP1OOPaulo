#include "ppm.hpp"
/*#include "filtros.hpp"
#include "filtro_negativo.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_pretoebranco.hpp"
#include "filtro_blur.hpp"*/

int main(){

	int choice;
	PPM *image = new PPM();
	image->read();
	if (image->getMagicNumber() != "P6") {
		cout << "Tipo de imagem não suportado." << endl;
		exit(0); 
	}
	else{
		cout << "1.Negativo " << "\n" << "2.Polarizado " << "\n" << "3.Preto e Branco " << "\n" << "4.Blur " << endl;
		cin >> choice;

		cout << "Numero mágico: " << image->getMagicNumber() << endl;
		cout << "Dimensões: " << image->getLargura() << "x" << image->getAltura() << endl;
		cout << "MaxColor: " << image->getMaxColor() << endl;

		if(choice==1){image->filtro_negativo();}	
		else if(choice==2){image->filtro_polarizado();}
		else if(choice==3){image->filtro_pretoebranco();}
		//else if(choice==4){image->filtro_blur();}
	}
	delete(image);

return 0;
}
