#include "ppm.hpp"

using namespace std;

void PPM::filtro_polarizado(){

 	string foto,nome;
    char blank='\n';

	cout << "Digite o nome de saída da imagem: " << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f2(nome.c_str());

    f2 << getMagicNumber();
    f2.put(blank);
    f2 << getLargura();
 	f2.put(blank);
 	f2 << getAltura();
    f2.put(blank);
    f2 << getMaxColor();
    f2.put(blank);

	for(int i=0; i<largura; i++){
	    for(int j=0; j<altura; j++){

           if((int)r[i][j] < 127){
               r[i][j] = 0;
			f2.write((char*)&r[i][j],1);
           }else{
               r[i][j] = maxcolor;
			f2.write((char*)&r[i][j],1);
           }


           if((int)g[i][j] < 127){
               g[i][j] = 0;
			f2.write((char*)&g[i][j],1);
           }else{
               g[i][j] = maxcolor;
			f2.write((char*)&g[i][j],1);
           }


           if((int)b[i][j] < 127){
               b[i][j] = 0;
			f2.write((char*)&b[i][j],1);
           }else{
               b[i][j] = maxcolor;
			f2.write((char*)&b[i][j],1);
           }
    	}
	}
	f2.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
